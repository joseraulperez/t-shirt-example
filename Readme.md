## T-Shirt source-code example

### Vue components
* Builder.vue: UI container
* ColorPicker.vue: Choose the color and emit the event.
* SizePicker.vue: Choose the size and emit the event.
* ImagesContainer.vue: It contain the images uploaded, you can change size and rotate them.
* ImageUploaded.vue: Upload images to s3 and add them to the t-shirt.
* Save.vue: Send the t-shirt finished to the endpoint.
* TShirt.vue: Receives all the events and change the t-shirt according to them.

### PHP part
* ImagesController.php: Endpoint to store the images in s3.
* ImagesTest.php: Controller test.