<?php

namespace App\Http\Controllers\AdminApi\Pod;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    /**
     * Store an image in the 'pod/images' folder and return the full url.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        return new JsonResponse(
            Storage::disk('s3')->url(
                $request->file->storePublicly('pod/images', 's3')
            )
        );
    }
}
