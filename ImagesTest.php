<?php

namespace Tests\Feature\AdminApi\Pod;

use App\Users\Notifications\Notification;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Tests\Concerns\InteractsWithUsers;
use DavidIanBonner\Presenter\Facades\Presenter;

class NotificationsTest extends TestCase
{
    use InteractsWithUsers;

    /** @return void */
    protected function setUp(): void
    {
        parent::setUp();

        $this->passportActingAs(
            $this->createUser()
        );
    }

    /** @test */
    function it_saves_an_image_in_the_storage()
    {
        Storage::fake('s3');

        $this->post(route('ajax.pod.images.store'), [
            'file' => UploadedFile::fake()->image('photo.jpg', 600, 600)
        ]);

        Storage::disk('s3')->assertHas('/pod/images/photo.jpg');
    }
}
